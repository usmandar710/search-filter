<?php

namespace FoodNow\SearchFilter;

use Illuminate\Support\ServiceProvider;

class SearchFilterServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Register your package's functionality here
    }

    public function boot()
    {
        // Perform any additional bootstrapping
    }
}
